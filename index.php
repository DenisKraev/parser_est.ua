<?php
// Парсер сайта est.ua
// Берет предложения по продаже
// Возвращает картинки для каждого объявления в отдельной папке с номером по порядку
// И сериализованный массив с данными объявлений в файле arr_ad.txt

set_time_limit(0);
ini_set('memory_limit', '1024m');
include 'simple_html_dom.php';

$arr = array(
    '106.187.53.47:9999',
);

$thumbIMG = 800;
$qualityIMG = 80;

function is_in_str($str, $substr) {
    $result = strpos ($str, $substr);
    if ($result === FALSE)
        return false;
    else
        return true;
}

function object2file($value, $filename) {
    $str_value = serialize($value);

    $f = fopen($filename, 'w');
    fwrite($f, $str_value);
    fclose($f);
}

function object_from_file($filename)
{
    $file = file_get_contents($filename);
    $value = unserialize($file);
    return $value;
}

//$arr = object_from_file('arr_proxy.txt');

//$rand_keys = array_rand ($arr);
//$rand_prxy = trim($arr[$rand_keys]);
//
//$auth = base64_encode('LOGIN:PASSWORD');
//
//$opts = array(
//    'http'=>array(
//        //'proxy' => '106.187.53.47:9999',
//        //'request_fulluri' => true,
//        'method'=>"GET",
//        'header'=> "User-Agent: Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36\r\n"
//    )
//);
//$context = stream_context_create($opts);

//$proxy = '106.187.53.47:9999';

function dlPage($href) {

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    //curl_setopt($curl, CURLOPT_PROXY, $proxy);
    curl_setopt($curl, CURLOPT_URL, $href);
    curl_setopt($curl, CURLOPT_REFERER, $href);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/533.4 (KHTML, like Gecko) Chrome/5.0.375.125 Safari/533.4");
    $str = curl_exec($curl);
    curl_close($curl);

    // Create a DOM object
    $dom = new simple_html_dom();
    // Load HTML from a string
    $dom->load($str);

    return $dom;
}

$get_city = 'kiev';

$start_link = 'http://'.$get_city.'.est.ua/nedvizhimost/kupit-kvartiru/?submitted=1&price_currency=uah'; // киев

//$html = file_get_html($start_link, false, $context);
$html = dlPage($start_link);

$pagin_spacer_last = $html->find('.paging__spacer__last', 0);
//print_r($html->find('.eo-list'));exit();
$countPages = $pagin_spacer_last->next_sibling()->plaintext; // количество страниц
//$countPages = 3900;
//$countPages = 9; // количество страниц

$numbAdd = 1; // номер объявления
// идем по всем страницам с объявлениями
for($i = 2; $i <= $countPages; $i++) {

    if($i <= 129) continue; // пропустить первые 21 страницу

    //$pageCardAd = file_get_html('http://kiev.est.ua/nedvizhimost/kupit-kvartiru/?submitted=1&price_currency=uah&p='.$i, false, $context);
    $pageCardAd = dlPage('http://'.$get_city.'.est.ua/nedvizhimost/kupit-kvartiru/?submitted=1&price_currency=uah&p='.$i); // киев
    //$pageCardAd = dlPage('http://cherkassy.est.ua/nedvizhimost/kupit-kvartiru/?submitted=1&price_currency=uah&p='.$i);

    $cardAd = $pageCardAd->find('.eo-list-item-supply');

    // идем по объявлениям на странице
    foreach($cardAd as $cardItem) {
        $linkAd = $cardItem->find('.eo-list-item-more-link a', 0)->href; // ссылка на страницу с объявлением

        //$pageAd = file_get_html($linkAd, false, $context); // на странице с объявлением
        $pageAd = dlPage($linkAd);

        // сбрасываем переменные
        $nameAd = null;
        $priceAd = null;
        $rooms_count = null;
        $square_general = null;
        $square_living = null;
        $square_kitchen = null;
        $floor = null;
        $floor_count = null;
        $region_full = null;
        $city_full = null;
        $area = null;
        $street_full = null;
        $prospectus_full = null;
        $lane_full = null;
        $house = null;
        $metro = null;
        $arrPhones = null;
        $arrImg = null;
        $description = null;

        //sleep(rand(1, 5));

        $nameAd = trim(str_replace("\xc2\xa0",' ',$pageAd->find('.column-left h1', 0)->plaintext)); // ! название объявления

        $pricesAd = $pageAd->find('.app-price-line .currencies span');
        foreach($pricesAd as $priceItem){
            if(trim($priceItem->plaintext) == 'долл'){
                $priceAd = trim(str_replace("\xc2\xa0",'',$priceItem->attr['data-price-total'])); // ! цена
            }
        }

        $strAddress = $pageAd->find('.location-block h3', 0)->next_sibling()->plaintext;
        //$strAddress = str_replace(chr(194),'',$strAddress);
        $strAddress = str_replace("\xc2\xa0",'',$strAddress);

        $arrAddress = explode(',', $strAddress);
        // ! данные адреса
        foreach($arrAddress as $addressItem) {
            if(is_in_str($addressItem, 'обл.') == true) $region_full = trim(str_replace('обл.', '', $addressItem));
            if(is_in_str($addressItem, 'г.') == true) $city_full = trim(str_replace('г.','', $addressItem));
            //if(is_in_str($addressItem, 'пгт.') == true) $village = trim(str_replace('пгт.','', $addressItem));
            if(is_in_str($addressItem, 'р-н') == true) $area = trim(str_replace('р-н','', $addressItem));
            if(is_in_str($addressItem, 'ул.') == true) $street_full = trim(str_replace('ул.','', $addressItem));
            if(is_in_str($addressItem, 'просп.') == true) $prospectus_full = trim(str_replace('просп.','', $addressItem));
            if(is_in_str($addressItem, 'пер.') == true) $lane_full = trim(str_replace('пер.','', $addressItem));
            if(preg_match("/^[\d]+$/",trim($addressItem)) == true) $house = trim($addressItem); // без букв у домов
        }

        // ! параметры квартиры
        foreach($pageAd->find('.info-blocks tr') as $param) {
            $nameParam = trim($param->find('th', 0)->plaintext);
            $valueParam = str_replace('&nbsp;',' ', trim($param->find('td', 0)->plaintext));

            if($nameParam == 'Количество комнат') $rooms_count = $valueParam;
            if($nameParam == 'Этаж') $floor = $valueParam;
            if($nameParam == 'Этажность') $floor_count = $valueParam;
            if($nameParam == 'Общая площадь') $square_general = trim(str_replace('м&sup2;','', $valueParam));
            if($nameParam == 'Жилая площадь') $square_living = trim(str_replace('м&sup2;','', $valueParam));
            if($nameParam == 'Площадь кухни') $square_kitchen = trim(str_replace('м&sup2;','', $valueParam));

            if($nameParam == 'Телефон') {
                $str = preg_replace("(<[^<>]+>)", '_', trim($param->find('td', 0)));
                $arrPhones = explode('_', trim($str)); // ! массив с телефонами
                $arrPhones = array_filter(
                    $arrPhones,
                    function($el){ return !empty($el);}
                );
            }
        }

        $description = $pageAd->find('.promo', 0)->plaintext; // ! описание

        $loc = $pageAd->find('.location-block', 0)->children(2)->plaintext;
        if(is_in_str($loc, '(M)') == true){
            preg_match('/\:(.+)\(/U', $loc, $matches);
            $metro = trim($matches[1]); // ! метро
        }

        if(is_in_str($pageAd->find('.slideshow-slides li img', 0)->attr['data-ico'], 'choke') != true) {
            $arrImg = array();
            foreach($pageAd->find('.slideshow-slides li') as $i2 => $slidItem) {
                $linkImg = str_replace("-preview-medium", '', $slidItem->find('img', 0)->attr['data-ico']); // ссылка на картинку

              $filepath = '/'.$get_city.'/images/'.$numbAdd.'/';

                mkdir($_SERVER['DOCUMENT_ROOT'].$filepath, 0777, true);

                $img_line = getimagesize ($linkImg);
                if ($img_line[2] == IMAGETYPE_JPEG) {
                    $type = 'jpg';

                    $iw = $img_line[0];
                    $ih = $img_line[1];
                    if($iw > $thumbIMG){
                        $src = ImageCreateFromJPEG($linkImg);

                        $koe = $iw/$thumbIMG;
                        $new_h = ceil($ih/$koe);
                        $dst = ImageCreateTrueColor($thumbIMG, $new_h);
                        ImageCopyResampled($dst, $src, 0, 0, 0, 0, $thumbIMG, $new_h, $iw, $ih);
                        ImageJPEG($dst, $_SERVER['DOCUMENT_ROOT'].$filepath.$i2.'.'.$type, $qualityIMG);
                        imagedestroy($src);
                    } else {
                        $s = file_get_contents($linkImg);

                        file_put_contents($_SERVER['DOCUMENT_ROOT'].$filepath.$i2.'.'.$type, $s);
                    }

                }
                if ($img_line[2] == IMAGETYPE_PNG) {
                    $type = 'png';

                    $iw = $img_line[0];
                    $ih = $img_line[1];
                    if($iw > $thumbIMG){
                        $src = ImageCreateFromPNG($linkImg);

                        $koe = $iw/$thumbIMG;
                        $new_h = ceil($ih/$koe);
                        $dst = ImageCreateTrueColor($thumbIMG, $new_h);
                        ImageCopyResampled($dst, $src, 0, 0, 0, 0, $thumbIMG, $new_h, $iw, $ih);
                        ImagePNG($dst, $_SERVER['DOCUMENT_ROOT'].$filepath.$i2.'.'.$type, $qualityIMG);
                        imagedestroy($src);
                    } else {
                        $s = file_get_contents($linkImg);

                        file_put_contents($_SERVER['DOCUMENT_ROOT'].$filepath.$i2.'.'.$type, $s);
                    }
                }
                if ($img_line[2] == IMAGETYPE_GIF) {
                    $type = 'gif';

                    $iw = $img_line[0];
                    $ih = $img_line[1];
                    if($iw > $thumbIMG){
                        $src = ImageCreateFromGIF($linkImg);

                        $koe = $iw/$thumbIMG;
                        $new_h = ceil($ih/$koe);
                        $dst = ImageCreateTrueColor($thumbIMG, $new_h);
                        ImageCopyResampled($dst, $src, 0, 0, 0, 0, $thumbIMG, $new_h, $iw, $ih);
                        ImageGIF($dst, $_SERVER['DOCUMENT_ROOT'].$filepath.$i2.'.'.$type, $qualityIMG);
                        imagedestroy($src);
                    } else {
                        $s = file_get_contents($linkImg);

                        file_put_contents($_SERVER['DOCUMENT_ROOT'].$filepath.$i2.'.'.$type, $s);
                    }
                }

                $arrImg[$i2] = $filepath.$i2.'.'.$type;

                if($i2 == 9) break;
            }
        } else {
            $arrImg = null;
        }

        $arrAd[$numbAdd] = array(
            'name' => $nameAd, //!
            'price' => $priceAd, //!
            'rooms_count' => $rooms_count, //!
            'square_general' => $square_general, //!
            'square_living' => $square_living, //!
            'square_kitchen' => $square_kitchen, //!
            'floor' => $floor, //!
            'floor_count' => $floor_count, //!
            'region_full' => $region_full, //!
            'city_full' => $city_full, //!
            'area' => $area, //!
            'street_full' => $street_full, //!
            'prospectus_full' => $prospectus_full, //!
            'lane_full' => $lane_full, //!
            'house' => $house, //!
            'metro' => $metro, //!
            'phones' => $arrPhones, //!
            'images' => $arrImg,
            'description' => trim($description),
        );

        object2file($arrAd, 'arr_ad.txt');

        $numbAdd++;

    }

    //if($i == 3) break;

}

//print_r(object_from_file('arr_ad.txt'));